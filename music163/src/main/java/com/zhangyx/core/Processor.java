package com.zhangyx.core;

import com.zhangyx.comm.RedisUtil;
import com.zhangyx.comm.ThreadUtil;
import org.apache.log4j.Logger;

public abstract class Processor extends Thread{
    private static Logger log = Logger.getLogger(Processor.class);
    /**
     * 间隔时间 毫秒
     * @return
     */
    public abstract int interval();

    /**
     * 处理核心
     */
    public abstract void core() throws Exception;
    public Page getNextPage(String key) {
        Page page = RedisUtil.popStackObj(key, Page.class);
        while (page == null) {
            log.info("缓存" + key + "中没有数据了，休息10秒");
            ThreadUtil.sleepMills(10000);
            page = RedisUtil.popStackObj(key, Page.class);
        }
        return page;
    }


    @Override
    public void run() {
        while (!interrupted()) {
            try {
                core();
            } catch (Exception e) {
                log.error("处理线程出现问题,休息1秒：" + e.getMessage());
                ThreadUtil.sleepMills(1000);
            }
            ThreadUtil.sleepMills(interval());
        }
    }
}
