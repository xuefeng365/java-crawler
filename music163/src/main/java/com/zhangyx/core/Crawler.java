package com.zhangyx.core;

import com.zhangyx.comm.RedisUtil;
import com.zhangyx.comm.ThreadUtil;
import org.apache.log4j.Logger;

public abstract class Crawler extends Thread{

    private static Logger log = Logger.getLogger(Crawler.class);
    /**
     * 抓取间隔时间-毫秒
     * @return
     */
    public abstract int intervalTime();

    /**
     * 下载网页
     * @return
     */
    public abstract Page download();

    /**
     * 处理网页--存缓存、验证网页是否正常
     * @param page
     */
    public abstract void deal(Page page);

    public Page getNextPage(String key) {
        Page page = RedisUtil.popStackObj(key, Page.class);
        while (page == null) {
            log.info("缓存" + key + "中没有数据了，休息1秒");
            ThreadUtil.sleepMills(1000);
            page = RedisUtil.popStackObj(key, Page.class);
        }
        return page;
    }

    @Override
    public void run() {
        while (!interrupted()) {
            deal(download());
            ThreadUtil.sleepMills(intervalTime());
        }
        System.exit(0);
    }

}
