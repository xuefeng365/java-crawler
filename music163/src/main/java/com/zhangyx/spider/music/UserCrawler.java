package com.zhangyx.spider.music;

import com.zhangyx.comm.RedisUtil;
import com.zhangyx.comm.ThreadUtil;
import com.zhangyx.core.Crawler;
import com.zhangyx.core.HttpClientUtil;
import com.zhangyx.core.Page;
import org.apache.log4j.Logger;

import static com.zhangyx.spider.music.Config.song_page_reponse;

public class UserCrawler extends Crawler {
    private static Logger log = Logger.getLogger(UserCrawler.class);

    @Override
    public int intervalTime() {
        return 0;
    }

    @Override
    public Page download() {
        Page page = getNextPage(Config.user_page_requst);
        HttpClientUtil client = HttpClientUtil.build(page.getUseGET(), page.getUrl())
                .addHeaders(page.getHeader())
                .addCookies(page.getRequestCookies(), "music.163.com", "/")
                .addEntity(page.getEntity())
                .start();
        page.setHtml(client.getHtml());
        page.setResponseCode(client.getResponseCode());
        return page;
    }

    @Override
    public void deal(Page page) {
        RedisUtil.pushQueueObj(Config.user_page_reponse, page);
    }
}
