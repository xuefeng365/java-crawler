package com.zhangyx.spider.music;

import com.alibaba.fastjson.JSONObject;
import com.zhangyx.comm.RedisUtil;
import com.zhangyx.comm.SerializeUtils;
import com.zhangyx.comm.ThreadUtil;
import com.zhangyx.core.Page;
import com.zhangyx.core.Processor;
import com.zhangyx.spider.music.entity.Comment;
import com.zhangyx.spider.music.entity.SongComment;
import com.zhangyx.spider.music.entity.User;
import org.apache.log4j.Logger;

import java.util.Map;

import static com.zhangyx.spider.music.Config.song_page_reponse;
import static com.zhangyx.spider.music.Config.song_page_requst;

public class SongProcessor extends Processor {
    private static Logger log = Logger.getLogger(SongProcessor.class);

    @Override
    public int interval() {
        return 10;
    }

    @Override
    public void core() throws Exception{
        Page page = getNextPage(Config.song_page_reponse);
        String songId = page.getTemp().get("song_id");
        SongComment songComment = JSONObject.parseObject(page.getHtml(), SongComment.class);
        if (songComment == null || songComment.getTotal() == 0) {
            log.info("解析失败，内容为：" + page.getHtml());
            return;
        }

        String repeat_key = page.getUrl() + "####" + songId;
        //下一页评论
        if (!RedisUtil.isExist(repeat_key)) {
            String para = "{\"rid\":\"R_SO_4_##\",\"offset\":\"**\",\"total\":\"false\",\"limit\":\"20\",\"csrf_token\":\"\"}";
            String baseUrl = "http://music.163.com/weapi/v1/resource/comments/R_SO_4_355961?csrf_token=";
            for(int i=20;i<songComment.getTotal();i=i+20) {
                String thisPata = para.replace("##", songId).replace("**", String.valueOf(i));
                Map<String, String> entityStr = Config.encrypt(thisPata);

                Page thispage = new Page();
                thispage.setUseGET(false);
                thispage.setUrl(baseUrl);
                thispage.setRequestCookies(Config.getCookies());

                Map<String, String> header = Config.getHeaders();
                header.put("Referer", "http://music.163.com/song?id=" + songId);

                thispage.setHeader(header);
                thispage.setEntity(entityStr);
                thispage.getTemp().put("song_id", songId);

                RedisUtil.pushStackObj(song_page_requst, thispage);
//                log.info("song --> comment ");
            }
            RedisUtil.set(repeat_key, "");
        }

        //用户详情页
        for (Comment comment : songComment.getComments()) {
            User user = comment.getUser();
            //去重
            if (RedisUtil.isExist("uid-" + user.getUserId())) {
                continue;
            }

            String baseUrl = "http://music.163.com/weapi/v1/play/record";
            String base_para = "{\"uid\":\"####\",\"type\":\"-1\",\"limit\":\"1000\",\"offset\":\"0\",\"total\":\"true\",\"csrf_token\":\"\"}";
            Map<String, String> paras = Config.encrypt(base_para.replace("####", String.valueOf(user.getUserId())));
            Map<String, String> header = Config.getHeaders();
            header.put("Referer", "http://music.163.com/user/home?id=" + user.getUserId());

            Page nextPage = new Page();
            nextPage.setUseGET(false);
            nextPage.setUrl(baseUrl);
            nextPage.setRequestCookies(Config.getCookies());
            nextPage.setHeader(header);
            nextPage.setEntity(paras);
            nextPage.getTemp().put("uid", String.valueOf(user.getUserId()));
            RedisUtil.pushStackObj(Config.user_page_requst, nextPage);

            RedisUtil.set("uid-" + user.getUserId(), "");
            log.info("song --> user " + user.getUserId() + "  " + user.getNickname() + "-->" + comment.getContent());
        }
    }
}
