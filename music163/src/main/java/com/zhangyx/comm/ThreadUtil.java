package com.zhangyx.comm;

public class ThreadUtil {

    public static void sleepMills(int mills) {
        try {
            Thread.sleep(mills);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
